import flatbuffers
import tflite.Model
import sys
import numpy as np

# maps common operations their its params size in bytes.
# to add an operations, run any tflite program, identify 
# the builtin data struct (check flatbuffer_conversions.cc) 
# and use the following debug expression: sizeof(YOURTYPE)
op_data_sizes = {
    0: 2, # add
    1: 20, # average pool 2d
    2: 4, # concatenation
    3: 12, # conv 2d 
    4: 14, # depthwise conv
    5: 2, # depth to space
    6: 0, # dequantize
    9: 6, # fully connected
    22: 18, # reshape
    25: 4, # softmax
    114: 0, # quantize
}

op_names = [
   "ADD","AVERAGE_POOL_2D","CONCATENATION","CONV_2D","DEPTHWISE_CONV_2D",
   "DEPTH_TO_SPACE","DEQUANTIZE","EMBEDDING_LOOKUP","FLOOR","FULLY_CONNECTED",
   "HASHTABLE_LOOKUP","L2_NORMALIZATION","L2_POOL_2D","LOCAL_RESPONSE_NORM","LOGISTIC",
   "LSH_PROJECTION","LSTM","MAX_POOL_2D","MUL","RELU",
   "RELU_N1_TO_","RELU","RESHAPE","RESIZE_BILINEAR","RNN",
   "SOFTMAX","SPACE_TO_DEPTH","SVDF","TANH","CONCAT_EMBEDDINGS",
   "SKIP_GRAM","CALL","CUSTOM","EMBEDDING_LOOKUP_SP","PAD",
   "UNIDIRECTIONAL_SEQU","GATHER","BATCH_TO_SPACE_ND","SPACE_TO_BATCH_ND","TRANSPOSE",
   "MEAN","SUB","DIV","SQUEEZE","UNIDIRECTIONAL_SEQU",
   "STRIDED_SLICE","BIDIRECTIONAL_SEQUE","EXP","TOPK_V","SPLIT",
   "LOG_SOFTMAX","DELEGATE","BIDIRECTIONAL_SEQUE","CAST","PRELU",
   "MAXIMUM","ARG_MAX","MINIMUM","LESS","NEG",
   "PADV","GREATER","GREATER_EQUAL","LESS_EQUAL","SELECT",
   "SLICE","SIN","TRANSPOSE_CONV","SPARSE_TO_DENSE","TILE",
   "EXPAND_DIMS","EQUAL","NOT_EQUAL","LOG","SUM",
   "SQRT","RSQRT","SHAPE","POW","ARG_MIN",
   "FAKE_QUANT","REDUCE_PROD","REDUCE_MAX","PACK","LOGICAL_OR",
   "ONE_HOT","LOGICAL_AND","LOGICAL_NOT","UNPACK","REDUCE_MIN",
   "FLOOR_DIV","REDUCE_ANY","SQUARE","ZEROS_LIKE","FILL",
   "FLOOR_MOD","RANGE","RESIZE_NEAREST_NEIG","LEAKY_RELU","SQUARED_DIFFERENCE",
   "MIRROR_PAD","ABS","SPLIT_V","UNIQUE","CEIL",
   "REVERSE_V","ADD_N","GATHER_ND","COS","WHERE",
   "RANK","ELU","REVERSE_SEQUENCE","MATRIX_DIAG","QUANTIZE",
   "MATRIX_SET_DIAG","ROUND","HARD_SWISH","IF","WHILE",
   "NON_MAX_SUPPRESSION","NON_MAX_SUPPRESSION"
]

def calculate_planned_area(model):
    """ recreation of the greedy memory planner algorithm to obtain max size """
    # since our models are linear, meaning the subgraph is a straight line 
    # with only 1 allocated tensor per layer. We don't need to implement
    # the full algorithm since the planned arena will just be the sum
    # of the consecutive tensors.

    subg = model.Subgraphs(0)

    def tensor_size_alligned_up(i):
        # tflite type enum to size in bytes
        type_sizes = {0:4,1:2,2:4,3:1,4:8,6:1,7:2,8:8,9:1}
        raw = np.prod(subg.Tensors(i).ShapeAsNumpy())
        extra = raw%16
        length = raw if extra==0 else 16*(np.math.floor(raw/16)+1)
        assert subg.Tensors(i).Type() in type_sizes, "Unsupported tflite type enum value {subg.Tensors(i).Type()}"
        return type_sizes[subg.Tensors(i).Type()] * length

    def needs_allocating(i):
        buffer_idx = subg.Tensors(i).Buffer()
        return model.Buffers(buffer_idx).DataLength() == 0 and not subg.Tensors(i).IsVariable()

    tensors = list(range(subg.TensorsLength()))
    # map to the format (tensors, size, first used, last used) for variable tensors only
    tensors = [[t,tensor_size_alligned_up(t),-1,-1] for t in tensors]
    
    assert subg.OutputsLength() == 1 and subg.InputsLength() == 1, "Subgraph has more than 1 input/output"
    tensors[subg.Outputs(0)][3] = subg.OperatorsLength()-1
    tensors[subg.Inputs(0)][2] = 0

    # find first used
    for o in range(subg.OperatorsLength()):
        for t in subg.Operators(o).OutputsAsNumpy():
            tensors[t][2] = o

    # find last used
    for o in range(subg.OperatorsLength()):
        for t in subg.Operators(o).InputsAsNumpy():
            tensors[t][3] = o

    # filter out tensors we dont need to allocate
    tensors = [t for t in tensors if needs_allocating(t[0])]

    # Use simplified algorithm described above
    # sort tensors by usage (in a way we can check that its linear)
    tensors.sort(key=lambda t: t[2] + 10000*t[3])
    assert len(tensors) > 0, "Model has no allcoated tensors. Is this a real model?"
    max_size = 0
    for i in range(len(tensors)-1):
        local_size = tensors[i][1] + tensors[i+1][1]
        if max_size < local_size:
            max_size = local_size

    return max_size


def calculate_arena_size(model):
    """
    Calculates the minimum tensor_arena size in bytes for a given tflite model
    to be run on the MSP430
    """
    subg = model.Subgraphs(0)
    tensor_len = subg.TensorsLength()
    operations_len = subg.OperatorsLength()

    node_and_registration_size = 0
    builtin_data_size = 0
    tensor_size = 0
    quantization_param_size = 0
    planned_area = calculate_planned_area(model)

    for i in range(operations_len):
        node_and_registration_size += 38 # Node and Registration
        operation_id = model.OperatorCodes(subg.Operators(i).OpcodeIndex()).BuiltinCode()
        operation_name = op_names[operation_id]
        assert operation_id in op_data_sizes, f"please add size of {operation_name}"
        tensor_builtin_data_size = op_data_sizes[operation_id]
        builtin_data_size += tensor_builtin_data_size
        # print(operation_name, tensor_builtin_data_size)

    for i in range(tensor_len):
        tensor_size += 50 # runtime tensor
        quant = subg.Tensors(i).Quantization()
        if quant:
            quantization_param_size += 12 # TfLiteAffineQuantization wrapper
            channels = quant.ScaleLength()
            quantization_param_size += 4*(channels+1) # zero points as a TfLiteIntArray
            quantization_param_size += 4*(channels+1) # scales as a TfLiteFloatArray

    total = node_and_registration_size + builtin_data_size + tensor_size + quantization_param_size + planned_area + 50
    print(f"node and registrations: {node_and_registration_size} bytes")
    print(f"builtin op data: {builtin_data_size} bytes")
    print(f"runtime tensors: {tensor_size} bytes")
    print(f"quantization params: {quantization_param_size} bytes")
    print(f"planned area: {planned_area} bytes")
    print(f"allignment overestimate: {50} bytes")
    print(f"total: {total}")

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Usage: python3 arena_size.py <pathtoyourmodel>")
        exit(1)
    try:
        with open(sys.argv[1],"rb") as f:
            raw = f.read()
            model = tflite.Model.Model.GetRootAsModel(raw, 0)
            calculate_arena_size(model)
    except FileNotFoundError:
        print(f"file {sys.argv[1]} doesn't exist")
        exit(1)
