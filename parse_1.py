import flatbuffers
import tflite.Model
from functools import reduce
import matplotlib.pyplot as plt


op_names = [
   "ADD","AVERAGE_POOL_2D","CONCATENATION","CONV_2D","DEPTHWISE_CONV_2D",
   "DEPTH_TO_SPACE","DEQUANTIZE","EMBEDDING_LOOKUP","FLOOR","FULLY_CONNECTED",
   "HASHTABLE_LOOKUP","L2_NORMALIZATION","L2_POOL_2D","LOCAL_RESPONSE_NORM","LOGISTIC",
   "LSH_PROJECTION","LSTM","MAX_POOL_2D","MUL","RELU",
   "RELU_N1_TO_","RELU","RESHAPE","RESIZE_BILINEAR","RNN",
   "SOFTMAX","SPACE_TO_DEPTH","SVDF","TANH","CONCAT_EMBEDDINGS",
   "SKIP_GRAM","CALL","CUSTOM","EMBEDDING_LOOKUP_SP","PAD",
   "UNIDIRECTIONAL_SEQU","GATHER","BATCH_TO_SPACE_ND","SPACE_TO_BATCH_ND","TRANSPOSE",
   "MEAN","SUB","DIV","SQUEEZE","UNIDIRECTIONAL_SEQU",
   "STRIDED_SLICE","BIDIRECTIONAL_SEQUE","EXP","TOPK_V","SPLIT",
   "LOG_SOFTMAX","DELEGATE","BIDIRECTIONAL_SEQUE","CAST","PRELU",
   "MAXIMUM","ARG_MAX","MINIMUM","LESS","NEG",
   "PADV","GREATER","GREATER_EQUAL","LESS_EQUAL","SELECT",
   "SLICE","SIN","TRANSPOSE_CONV","SPARSE_TO_DENSE","TILE",
   "EXPAND_DIMS","EQUAL","NOT_EQUAL","LOG","SUM",
   "SQRT","RSQRT","SHAPE","POW","ARG_MIN",
   "FAKE_QUANT","REDUCE_PROD","REDUCE_MAX","PACK","LOGICAL_OR",
   "ONE_HOT","LOGICAL_AND","LOGICAL_NOT","UNPACK","REDUCE_MIN",
   "FLOOR_DIV","REDUCE_ANY","SQUARE","ZEROS_LIKE","FILL",
   "FLOOR_MOD","RANGE","RESIZE_NEAREST_NEIG","LEAKY_RELU","SQUARED_DIFFERENCE",
   "MIRROR_PAD","ABS","SPLIT_V","UNIQUE","CEIL",
   "REVERSE_V","ADD_N","GATHER_ND","COS","WHERE",
   "RANK","ELU","REVERSE_SEQUENCE","MATRIX_DIAG","QUANTIZE",
   "MATRIX_SET_DIAG","ROUND","HARD_SWISH","IF","WHILE",
   "NON_MAX_SUPPRESSION","NON_MAX_SUPPRESSION"
]

def get_plt(buf):
    buf = bytearray(buf)
    model = tflite.Model.Model.GetRootAsModel(buf, 0)

    subg = model.Subgraphs(0)
    tensors_len = subg.TensorsLength()
    tensors = [subg.Tensors(x) for x in range(tensors_len)]

    x_names = []
    y_sizes = []

    for tensor in tensors:
        name = tensor.Name().decode()
        type_size = {0:4,9:1}[tensor.Type()]
        ttype = {0:'float',9:'uint8'}[tensor.Type()]
        size = reduce(lambda a,b: a*b,[tensor.Shape(x) for x in range(tensor.ShapeLength())]) * type_size
        print(f'name: {name}')
        print(f'\t shape: {[tensor.Shape(x) for x in range(tensor.ShapeLength())]}')
        print(f'\t total: {size}')
        print(f'\t type: {ttype}')

        x_names.append(f'{name} ({size})' if size >= 400 else '')
        y_sizes.append(size)

    return x_names, y_sizes



print('----------FLOAT--------')
buf = open('model.tflite', 'rb').read()
names_1, sizes_1 = get_plt(buf)
# buf = bytearray(buf)
# model = tflite.Model.Model.GetRootAsModel(buf, 0)
# subg = model.Subgraphs(0)
# operators=[subg.Operators(x) for x in range(subg.OperatorsLength())]
# for operator in operators:
#     print(op_names[operator.OpcodeIndex()])
#     print(f'inputs: {[operator.Inputs(x) for x in range(operator.InputsLength())]}')
#     print(f'outputs: {[operator.Outputs(x) for x in range(operator.OutputsLength())]}')

total_1 = reduce(lambda x,y: x+y,sizes_1)
print('----------QUANTIZED--------')
buf = open('model_quantized.tflite', 'rb').read()
names_2, sizes_2 = get_plt(buf)
total_2 = reduce(lambda x,y: x+y,sizes_2)

# fig, (ax1, ax2) = plt.subplots(2)
# ax1.pie(sizes_1,labels=names_1)
# ax1.set_title(f'float model, total = {total_1}')
# ax2.pie(sizes_2,labels=names_2)
# ax2.set_title(f'quantized model, total = {total_2}')
# plt.show()